#include <iostream>
#include <iomanip>
#include "primes.h"

using namespace std;

inline unsigned long long int ratio(unsigned long long int a, unsigned long long int b) {
  return (b > 10) ? (100 * a)/b : 100;
}

int main() {
  prime p(5);
  unsigned long long int max_size = 2280;

  unsigned long long int mid = max_size / 2;
  
  unsigned long long int next = 1;

  unsigned long long int i = mid;
  unsigned long long int j = mid;
  bool changej = true;
  unsigned long long int offset = 1;
  unsigned long long int joffset = 0;
  unsigned long long int ioffset = 0;
  unsigned long long int max;
  unsigned long long int moves = 0;
  unsigned long long int last_moves = 1;
  unsigned long long int checked = 0;
  unsigned long long int primes = 0;
  unsigned long long int size;
  unsigned long long int end = -1;
  for(size = 1; size < max_size; size += 2) {
    max = size * size;
    for(; next <= max; next++) {
      if((i == j) || ((i + j) == (mid + mid))) {
	checked++;
	end++;
	if (p.isPrime(next)) {
	  primes++;
	}
	if((end % 4) == 0) {
	  // cout << "size = " << size << ", next = " << next << endl;
	  if(ratio(primes, checked) < 10) {
	    cout << "found " << primes << " primes of " << checked << " checked; size= " << size << endl;
	    cout << "(ratio=" << ratio(primes, checked) << ")" << endl;
	    cout << "next=" << next << "; sqrt(next) = " << sqrt(next) << endl;
	    cout << "sqrt(next)^2=" << sqrt(next) * sqrt(next) << endl;
	    return 0;
	  }
	}
      }
      moves++;
      if(changej) {
	j += offset;
      } else {
	i += offset;
      }
      if(moves == last_moves) {
	changej = !changej;
	if(changej) {
	  offset = -offset;
	  last_moves++;
	}
	moves = 0;
      }
    }
  }
  
  cout << "final ratio: " << ratio(primes, checked) << " with " << primes << " primes found of " << checked << "; size=" << size << endl;
  
  return 0;
}
  
