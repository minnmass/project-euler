#include <iostream>
#include <vector>

using namespace std;

int main() {
  vector<unsigned int> factor;

  unsigned int triangle = 1;
  for(unsigned int i = 2; i > 0; i++) {
    triangle += i;

    factor.clear();
    for(unsigned int j = 1; j <= (triangle / 2); j++) {
      if(!(triangle % j)) {
	factor.push_back(j);
      }
    }
    factor.push_back(triangle);

    if((i % 100) == 0) {
      cout << "triangle " << i << " is " << triangle << ", with " << factor.size() << " factors" << endl;
    }

    if(factor.size() > 500) {
      cout << "num factors: " << factor.size() << endl;
      cout << "factors: ";
      for(int j = 0; j < factor.size(); j++) {
	cout << factor[j] << " ";
      }
      cout << '\n';
      cout << triangle << endl;
      return 0;
    }
  }

  cout << "error" << endl;
  return -1;
}
