#include <iostream>
#include <vector>

using namespace std;

int main() {
  vector<unsigned int> v;
  unsigned int n;
  bool done;
  unsigned int length;

  v.push_back(0);
  v.push_back(1);
  for(unsigned int i = 2; i < 1000000; ++i) {
    n = i;
    done = false;
    length = 0;
    while(!done) {
      if(n % 2) {
	//i odd
	n = (3*n) +1;
      } else {
	n = n / 2;
      }
      length++;
      if(v.size() > n) {
	v.push_back(length + v[n]);
	done = true;
      } else if (n == 1) {
	v.push_back(length);
      }
    }
  }

  cout << "tried " << v.size() << " lengths" << endl;
  
  unsigned int idx = 0;
  for(unsigned int i = 0; i < v.size(); i++) {
    if(v[i] > v[idx]) {
      idx = i;
    }
  }

  cout << "answer: " << idx << ", with " << v[idx] << " length." << endl;
  return 0;
}
