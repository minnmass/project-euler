#include <iostream>
#include <list>
using namespace std;

/*
bool loop(list<unsigned long long int> l, int key) {
  list<unsigned long long int>::iterator iter;
  for(iter = l.begin(); iter != l.end(); ++iter) {
    if(*iter == key) {
      return true;
    }
  }
  return false;
}
*/

int main() {
  cout << "starting running" << endl;
  int fact[10];
  fact[0] = 1;
  fact[1] = 1;
  for(int i = 2; i < 10; i++) {
    fact[i] = fact[i-1] * i;
  }

  list<unsigned long long int> l;

  int in60 = 0;
  int temp;
  int sum;
  bool loop;
  for(int i = 0; i < 1000000; i++) {
    l.clear();
    temp = i;
    sum = i;
    do {
      loop = false;
      l.push_back(sum);
      sum = 0;
      while(temp > 0) {
	sum += fact[temp % 10];
	temp /= 10;
      }
      temp = sum;
      for(list<unsigned long long int>::iterator iter = l.begin(); iter != l.end(); ++iter) {
	if(*iter == sum) {
	  loop = true;
	}
      }
    } while(!loop);
    if(l.size() == 60) {
      in60++;
    }
  }

  cout << "found " << in60 << " 60-long loops" << endl;
  return 0;
}
