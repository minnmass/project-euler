#include <iostream>
#include <string>
#include <algorithm>

using namespace std;

string toSortedString(unsigned long long int x) {
  string s;
  char c;
  while(x > 0) {
    c= (x%10) + '0';
    s = c + s;
    x /= 10;
  }
  sort(s.begin(), s.end());
  return s;
}

int main() {
  string s2;
  string s3;
  string s4;
  string s5;
  string s6;

  unsigned long long int x = 0;
  bool done = false;
  while(!done) {
    x++;
    s2 = toSortedString(2*x);
    s3 = toSortedString(3*x);
    if(s2 == s3) {
      s4 = toSortedString(4*x);
      if(s2 == s4) {
	s5 = toSortedString(5*x);
	if(s2 == s5) {
	  s6 = toSortedString(6*x);
	  if(s2 == s6) {
	    done = true;
	  }
	}
      }
    }
  }

  cout << x << endl;
  return 0;
}
