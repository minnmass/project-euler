#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

int main() {
  vector<unsigned int> primes;
  int num_sum = 0;
  int max_idx;
  
  primes.push_back(2);
  bool possible;
  for(int i = 3; i < 1000000; i++) {
    possible = true;
    for(int j = 0; (j < primes.size()) && possible; j++) {
      if((i%primes[j]) == 0) {
	possible = false;
      }
    }
    if(possible) {
      primes.push_back(i);
    }
  }

  ofstream outs;
  outs.open("primes_under_1_million.txt");
  if(!outs) {
    cerr << "Error opening outs" << endl;
  } else {
    cerr << "opened outs, dumping primes" << endl;
    for(int i = 0; i < primes.size(); i++) {
      outs << primes[i] << '\n';
    }
    outs.close();
    cerr << "done" << endl;
  }

  int total;
  int idx;
  for(int i = primes.size() - 1; i >= 0; --i) {
    total = 0;
    for(int j = 0; j < primes.size(); j++) {
      idx = j;
      while (total < primes[i]) {
	total += primes[idx];
	++idx;
      }
      if(total == primes[i]) {
	if((idx - j) > num_sum) {
	  num_sum = (idx - j);
	  max_idx = i;
	}
      }
    }
  }

  cout << "answer: " << primes[max_idx] << ", with " << num_sum << " consecutive primes" << endl;
  return 0;
}
