#include <iostream>
#include "primes.h"
#include <math.h>
using namespace std;

int main () {
  int foo[12]={3,1,7,5,8,4,9,3,1,8,0,3};
  unsigned long long int x = 0;
  for(int i = 0; i < 12; i++) {
    x *= 10;
    x += foo[i];
  }

  prime p(sqrt(x));

  for(int i = p.size() - 1; i >= 0; i--) {
    if((x % p.at(i)) == 0) {
      cout << "largest prime factor: " << p.at(i) << " (i=" << i << ")" << endl;
      return 0;
    }
  }

  cout << "Error: no prime factors found" << endl;

  return 0;
}

