#include <iostream>
#include <vector>
#include <string>
#include <list>
#include <algorithm>

using namespace std;

bool possible(string s, int required = 1) {
  int lex_left = 0;

  // basically "while (lex_left <= required)" but with bounds-proofing
  for(int i = 1; (i < s.size()) && (lex_left <= required); i++) {
    // if the element to the left of this element is "left" alphabetically
    if(s[i-1] < (s[i])) {
      // incriment the number of such elements in the string
      lex_left++;
    }
  }

  // this string works iff we have exactly <required> matches
  //return (lex_left == required);
  if(lex_left != required) {
    //    cout << s << " definitely not" << endl;
    return false;
  }
  //but, we must also check that there aren't too many of the same letter
  string temp = s;
  //  cout << s << "=?=" << temp << "     ";
  sort(temp.begin(), temp.end());
  // and, because "unique()" isn't working...
  //  cout << s << "=?=" << temp << "     ";
  for(int i = 1; i < temp.length(); i++) {
    if(temp[i-1] == temp[i]) {
      //      cout << "well, no" << endl;
      return false;
    }
  }
  //  cout << "yes!" << endl;
  return true;
}

int main() {
  //for a-z, trivally, either 0 or 26; either way, less than p(3)=10400
  //anyway
  //for each string in the previous loop (begin with a-z)
  //  for a-z, do
  //    append letter
  //    if still no longer a possibility
  //      remove this string from consideration
  //    otherwise, 
  //      if the length of this string is <= MAX_LENGTH
  //        incriment p(this string's length)
  //        preppend this new string to the list of candidates 
  //        (appending will result in an infinite loop?)
  //      otherwise,
  //        discard this string (it's too long for us to care about)
  //note: we'll keep looping until the list is empty; we'll either
  //discard the string due to length, or due to failed expansion
  
  int maxLength = 7;
  char endChar = 'Z';
  list<string> maybe;
  vector<unsigned long long int> p(maxLength + 1, 0); // we want to be able to check p[maxLength]
                                   // also, initialize everything to 0
  string temp; // scratch string
  
  for(char i = 'A'; i <= endChar; i++) {
    for(char j = 'A'; j <= endChar; j++) {
      temp = i;
      temp = temp + j;
      maybe.push_back(temp);
    }
  }
  //reminder "for looping" over the list:
  //  for(iter = maybe.begin(); iter != maybe.end(); iter++) {

  list<string>::iterator iter; // = maybe.begin();
  //  for(iter = maybe.begin(); iter != maybe.end(); iter++) {
  int last_length = 0;
  unsigned long long int max = 0;
  iter = maybe.begin(); while(!maybe.empty()) {
    if(iter->length() != last_length) {
      cout << "starting next length (" << iter->length() << "; " << *iter << ")" << endl;
      last_length = iter->length();
      if(p[iter->length()] > max) {
	max = p[iter->length()];
      }
      cout << "p(" << iter->length() << ")=" << p[iter->length()] << endl;
    }
    for(char i = 'A'; i <= endChar; i++) {
      temp = *iter + i;
      if(temp.length() <= maxLength) {
	if(possible(temp, 1)) {
	  maybe.push_back(temp);
	  ++p[temp.length()];
	}
      }
    }
    iter = maybe.erase(iter);
  }

  cout << "maximum is " << max << endl;
  return 0;
}
