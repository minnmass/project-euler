#include <iostream>
#include "primes.h"
#include <string>
#include <algorithm>

using namespace std;

int toInt(string s) {
  int x = 0;
  for(int i = 0; i < s.size(); i++) {
    x *= 10;
    x += s[i] - '0';
  }
  return x;
}

string toString(int x) {
  string s;
  char c;
  while(x > 0) {
    c = (x % 10) + '0';
    s = c + s;
    x /= 10;
  }
  return s;
}

string next(string s) {
  string ret = s.substr(1);
  ret += s[0];
  return ret;
}

int main() {
  unsigned long long int max = 1000000;
  prime p(max);
  cout << "done building primes (last is) ";
  cout << p.at(p.size() - 1) << endl;
  cout << "                               9999991" << endl;
  cout << "there are " << p.size() << " primes" << endl;

  unsigned long long int cur;
  string s;
  bool possible;
  int total = 0;
  int testing;
  unsigned long long int i;
  for(i = 0; (i < p.size()) && (p.at(i) < max); i++) {
    possible = true;
    cur = p.at(i);
    s = toString(cur);
    do {
      s = next(s);
      testing = toInt(s);
      if(testing == 999991) {
	cout << "999991" << endl;
	cout << p.at(p.size() - 1) << endl;
	cout << "there are " << p.size() << " primes" << endl;
      }
      possible = p.isPrime(testing, (testing == 999991));
    } while (possible && (testing != cur));

    if(possible) {
      total++;
    }
  }

  cout << i << "=i" << endl;
  cout << p.at(i-1) << endl;
  cout << total << " circular primes below " << max << endl;
  return 0;
}
