#include <iostream>

using namespace std;

inline int square(int x) {
  return x*x;
}

int main() {
  int a, b, c;

  int i, j, k;
  for(i = 1; i < 1000; i++) {
    for(j = 1; j < 1000; j++) {
      for(k = 1; k < 1000; k++) {
	a = square(i);
	b = square(j);
	c = square(k);

	if((a+b) == c) {
	  if((i + j + k) == 1000) {
	    cout << i << ", " << j << ", " << k << "= " << i * j * k << endl;
	    return 0;
	  }
	}
      }
    }
  }

  cout << "error" << endl;
  return 1;
}
