#include <iostream>

using namespace std;

//cheated; used algorithm from http://mathforum.org/library/drmath/view/54333.html
//but, modified for our denominations

int p(int n)
{
  if(n > 0)  return 1;
  return 0;
}

int p2(int x) {
  if(x >= 0) return p2(x-1) + p(x);
  return 0;
}

int n(int x)
{
  if (x >= 0) return n(x-2) + p(x-3) + p2(x);
  return 0;
}

int d(int x)
{
  if (x >= 0) return d(x-10) + n(x);
  return 0;
}

int q(int x)
{
  if (x >= 0) return q(x-20) + d(x-10)+ d(x);
  return 0;
}

int c(int x)
{
  if (x >= 0) return c(x-50) + q(x);
  return 0;
}

int c2(int x) {
  if (x >= 0) return c2(x-100) + c(x);
  return 0;
}

int main()
{
  cout << c2(100) << endl;
}

/*
int called[9];

int ways(int value) {
  called[0]++;
  switch(value) {
  case 1:
    called[1]++;
    return 1;
  case 2:
    called[2]++;
    return ways(1) + ways(1) + 1;
  case 5:
    called[3]++;
    return ways(2) + ways(2) + ways(1) + 1;
  case 10:
    called[4]++;
    return ways(5) + ways(5) + 1;
  case 20:
    called[5]++;
    return ways(10) + ways(10) + 1;
  case 50:
    called[6]++;
    return ways(20) + ways(20) + ways(10) + 1;
  case 100:
    called[7]++;
    return ways(50) + ways(50) + 1;
  case 200:
    called[8]++;
    return ways(100) + ways(100) + 1;
  }
  return 0;
}

int main() {
  for(int i = 0; i < 9; i++) {
    called[i] = 0;
  }
  cout << ways(200) << endl;
  for(int i = 0; i < 9; i++) {
    cout << i << " = " << called[i] << endl;
  }
  return 0;
}

*/
