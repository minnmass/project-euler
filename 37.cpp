#include <iostream>
#include "primes.h"
#define uint unsigned long long int

using namespace std;

uint truncateLeft(uint x) {
  uint acc = 0;
  uint pow = 1;

  while(x > 10) {
    acc += (x % 10) * pow;
    pow *= 10;
    x /= 10;
  }

  return acc;
}

uint truncateRight(uint x) {
  return x / 10;
}

bool isRightTrunctable(uint x, prime *p) {
  do {
    if(!p->isPrime(x)) {
      return false;
    }
    x = truncateRight(x);
  } while(x > 0);
  return true;
}

bool isLeftTrunctable(uint x, prime *p) {
  do {
    if(!p->isPrime(x)) {
      return false;
    }
    x = truncateLeft(x);
  } while (x > 0);
  return true;
}

int main() {
  prime p(750000);

  int primesFound = 0;
  int i = 11;
  uint answer = 0;
  while(primesFound < 11) {
    if(isRightTrunctable(i, &p) && isLeftTrunctable(i, &p)) {
      cout << i << " is trunctably prime" << endl;
      ++primesFound;
      answer += i;
    }
    i += 2;
  }

  cout << "The answer is: " << answer << "." << endl;
}
