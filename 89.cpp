#include <iostream>
#include <string>

using namespace std;

int value(char c) {
  switch (c) {
  case 'I':
    return 1;
  case 'V':
    return 5;
  case 'X':
    return 10;
  case 'L':
    return 50;
  case 'C':
    return 100;
  case 'D':
    return 500;
  case 'M':
    return 1000;
  }
  return 0;
}

int value(string s) {
  cout << s << endl;
  int val = 0;
  int temp = value(s[0]);

  char current;
  char last = s[0];

  for(int i = 1; i < s.size(); i++) {
    current = s[i];
    cout << "current=" << current << ", last=" << last << endl;
    cout << "temp=" << temp << ", val=" << val << endl;
    if(value(current) > value(last)) {
      cout << "  increasing val by " << value(current) - temp << endl;
      val += value(current) - temp;
      temp = 0;
    } else if(value(current) < value(last)) {
      cout << "  increasing val by " << temp << endl;
      val += temp;
      temp = 0;
    } else { // same character as last time
      cout << "  increasing temp by " << value(current) << endl;
      temp += value(current);
    }
  }
  cout << "done looping, increasing val by " << temp << endl;
  val += temp; // should be protected by if(temp != 0), but...
  return val;
}

int main() {
  for(char i = 'A'; i <= 'Z'; i++) {
    if(value(i) != 0) {
      cout << i << " = " << value(i) << endl;
    }
  }
  cout << value("XIV") << endl;
  return 0;

  string s[] = {"IIIIIIIIIIIIIIII", "VIIIIIIIIIII", "VVIIIIII", "XIIIIII", "VVVI", "XVI", "XIV", "DONE"};
  int i = 0;
  while(s[i] != "DONE") {
    cout << s[i] << " = " << value(s[i]) << endl;
    i++;
  }
  return 0;
}
