#include <iostream>
#include "primes.h"

using namespace std;

int main() {
  prime p;

  cout << p.size() << endl;

  cout << p.at(10000) << endl;

  while (p.size() < 10000) {
    if(!p.add()) {
      cout << "highest prime: " << p.at(p.size() - 1) << " at " << p.size() << endl;
      return 1;
    }
  }

  cout << p.at(10000) << endl;
  return 0;
}
