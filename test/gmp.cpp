//g++ -o gmp -lgmpxx -lgmp gmp.cpp

#include <iostream>
#include <gmpxx.h>

typedef mpz_class longInt;

using namespace std;

longInt reverseOf(longInt l) {
  longInt ret = 0;
  while(l > 0) {
    ret *= 10;
    ret += l % 10;
    l /= 10;
  }
  return ret;
}

bool palendrome(longInt x) {
  return (x == reverseOf(x));
}

int main() {
  longInt a;
  int lychrel = 0;
  int iterations = 0;
  int max = 10000;
  int index;

  for (index = 0; index < max; index++) {
    a = index;
    iterations = 0;
    do {
      iterations++;
      a += reverseOf(a);
    }while((!palendrome(a)) && (iterations <= 51));
    if(iterations > 50) {
      cout << index << " appears to be lychrell; got " << a << endl;
      lychrel++;
    }
  }

  cout << lychrel << " lychrell numbers found." << endl;
  return 0;
}
