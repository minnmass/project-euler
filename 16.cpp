#include <iostream>
#include <fstream>

using namespace std;

int main() {
  int sum = 0;
  char c;
  int n;
  ifstream ins;

  ins.open("16.start");
  if(!ins) {
    cerr << "error opening file, bailing" << endl;
    exit(-1);
  }

  ins >> c;
  while (ins.peek() != EOF) {
    n = c - '0';
    sum += n;
    ins >> c;
  }

  cout << sum << endl;
  return 0;
}
