#include <iostream>

using namespace std;

#define NUMS 20

bool good(unsigned int n) {
  for(unsigned int i = 2; i <= NUMS; i++) {
    //    cout << "checking " << n << "%" << i << "= " << n%i << " ";
    if(n%i) {
      //      cout << "returning zero because of " << i << endl;
      return 0;
    }
    //    cout << "next ";
  }
  return 1;
}

int main () {
  unsigned int x = 1;
  for(unsigned int i = 1; i <= NUMS; i++) {
    x *= i;
  }

  cout << "checking up to " << x << endl;

  for(unsigned int i = NUMS; i <=x; i++) {
    //    cout << "checking " << i << "...";
    if(good(i)) {
      cout << i << endl;
      return 0;
    }
  }

  cout << "error" << endl;
  return 0;
}
