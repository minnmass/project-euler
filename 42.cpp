#include <iostream>
#include <fstream>
#include <vector>
#include <string>

using namespace std;

int getVal(char c) {
  return (c - 'A') + 1;
}

vector<unsigned int> tri;

void addTriangle() {
  tri.push_back(tri[tri.size()-1] + tri.size());
}

bool isTriangle(int n) {
  while(tri[tri.size()-1] < n) {
    addTriangle();
  }

  for(int i = 0; i < tri.size(); i++) {
    if(tri[i] == n) {
      return 1;
    }
  }
  return 0;
}

int main() {
  ifstream ins;
  ins.open("words42.txt");
  if(!ins) {
    cout << "Error opening file, bailing" << endl;
  }

  tri.push_back(0);
  for(int i = 1; i < 50; i++) {
    addTriangle();
  }
  
  vector<string> words;
  string temp;
  getline(ins, temp);
  
  string::size_type loc = temp.find_first_of('\"');
  while(loc != string::npos) {
    temp.erase(loc, 1);
    loc = temp.find_first_of('\"');
  }
  
  while(!temp.empty()) {
    loc = temp.find_first_of(',');
    if(loc == string::npos) {
      words.push_back(temp);
      temp.erase();
    } else {
      words.push_back(temp.substr(0, loc));
      temp.erase(0, loc + 1);
    }
  }

  cout << "considering " << words.size() << " words." << endl;

  int triangles = 0;
  int total;
  for(int i = 0; i < words.size(); i++) {
    total = 0;
    temp = words[i];
    for(int j = 0; j < temp.size(); j++) {
      total += getVal(temp[j]);
    }
    if(isTriangle(total)) {
      triangles++;
    }
  }

  cout << triangles << endl;

  return 0;
}
