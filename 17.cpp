#include <iostream>
#include <string>
using namespace std;

string getStr(int number) {
  switch (number) {
  case 0:
    return "";
  case 1:
    return "one";
  case 2:
    return "two";
  case 3:
    return "three";
  case 4:
    return "four";
  case 5:
    return "five";
  case 6:
    return "six";
  case 7:
    return "seven";
  case 8:
    return "eight";
  case 9:
    return "nine";
  case 10:
    return "ten";
  case 11:
    return "eleven";
  case 12:
    return "twelve";
  case 13:
    return "thirteen";
  case 14:
    return "fourteen";
  case 15:
    return "fifteen";
  case 18:
    return "eighteen";
  case 16:
  case 17:
    //  case 18:
  case 19:
    return getStr(number % 10) + "teen";
  }
  if(number < 30) {
    return "twenty" + getStr(number % 10);
  }
  if(number < 40) {
    return "thirty" + getStr(number % 10);
  }
  if(number < 50) {
    return "fourty" + getStr(number % 10);
  }
  if(number < 60) {
    return "fifty" + getStr(number % 10);
  }
  if(number < 70) {
    return "sixty" + getStr(number % 10);
  }
  if(number < 80) {
    return "seventy" + getStr(number % 10);
  }
  if(number < 90) {
    return "eighty" + getStr(number % 10);
  }
  if(number < 100) {
    return "ninty" + getStr(number % 10);
  }
  if(number < 1000) {
    if((number % 100) == 0) {
      // x00
      return getStr(number / 100) + "hundred";
    }
    // xxx, not x00
    return getStr(number / 100) + "hundredand" + getStr(number % 100);
  }
  if (number == 1000) {
    return "onethousand";
  }
  return "error";
}

// count the number of letters when spelling out numbers
// use the British spelling, which puts "and" before the 10s/1s column
// eg. one hundred and sixty-two or two thousand and three
// do not count spaces or hyphens
int main() {
  unsigned long long int letters = 0;
  for (int i = 1; i < 1001; i++) {
    // cout << i << ":  " << getStr(i) << endl;
    //    cout << "  length: " << getStr(i).length() << endl;
    letters += getStr(i).length();
    //    cout << "  total: " << letters << endl;
  }
  cout << "Found " << letters << " letters total.";
}
