#include <iostream>
#include <math.h>

// #dne L 1000000
#define L 10000

using namespace std;

int main() {
  int a, b, l;
  double c;
  int solutions = 0;

  bool found1 = 0;
  bool found2 = 0;
  for(l = 1; l <= L; l++) {
    for(a = 1; a < l/2; a++) {
      for(b = 1; b <= a; b++) {
	c = sqrt((a*a) + (b*b));
	if(a + b + c == l) {
	  if(found1) {
	    found2 = true;
	    cout << l << " doesn't work " << '\n';
	    a = l;
	    b = a * 2;
	  }
	  found1 = true;
	}
      }
    }
    if(found1 && !found2) {
      ++solutions;
    }
    found1 = false;
    found2 = false;
  }

  cout << "found: " << solutions << endl;
  return 0;
}
