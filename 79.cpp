#include <iostream>
#include <stack>

using namespace std;

int main() {
  stack<int> data;
  data.push(319);  data.push(680);  data.push(180);  data.push(690);  data.push(129);  data.push(620);  data.push(762);  data.push(689);  data.push(762);  data.push(318);  data.push(368);  data.push(710);  data.push(720);  data.push(710);  data.push(629);  data.push(168);  data.push(160);  data.push(689);  data.push(716);  data.push(731);  data.push(736);  data.push(729);  data.push(316);  data.push(729);  data.push(729);  data.push(710);  data.push(769);  data.push(290);  data.push(719);  data.push(680);  data.push(318);  data.push(389);  data.push(162);  data.push(289);  data.push(162);  data.push(718);  data.push(729);  data.push(319);  data.push(790);  data.push(680);  data.push(890);  data.push(362);  data.push(319);  data.push(760);  data.push(316);  data.push(729);  data.push(380);  data.push(319);  data.push(728);  data.push(716);

  bool grid[10][10]; // if (x, y) is true, x comes before y
  for(int i = 0; i < 10; i++) {
    for(int j = 0; j < 10; j++) {
      grid[i][j] = false;
    }
  }

  int val;
  int a; int b; int c;
  while(!data.empty()) {
    val = data.top();
    data.pop();

    c = val % 10; val /= 10;
    b = val % 10; val /= 10;
    a = val;

    grid[a][b] = true;
    grid[b][c] = true;
    grid[a][c] = true;
  }

  for(int i = 0; i < 10; i++) {
    for(int j = 0; j < 10; j++) {
      if(grid[i][j]) {
	cout << i << " before " << j << endl;
      }
    }
  }

  return 0;
}

/*

7 3 1 6 2 8 9 0


*/
