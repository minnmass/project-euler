#include <iostream>
#include <limits>
using namespace std;

// #define SMALL 0

#ifdef SMALL
#define SIZE 5

long long int matrix[SIZE][SIZE]= {
  #include "81.matrix.small.txt"
};

#else

#define SIZE 80

long long int matrix[SIZE][SIZE]= {
  #include "81.matrix.txt"
};
#endif

inline long long int min(long long int a, long long int b) {
  return a < b ? a : b;
}

inline long long int getValueAbove(int x, int y) {
  return y > 0 ? matrix[x][y-1] : numeric_limits<long long int>::max();
}

inline long long int getValueLeft(int x, int y) {
  return x > 0 ? matrix[x-1][y] : numeric_limits<long long int>::max();
}

void printMatrix() {
  for (int x = 0; x < SIZE; ++x) {
    for (int y = 0; y < SIZE; ++y) {
      cout << matrix[x][y] << " ";
    }
    cout << endl;
  }
  cout << "**********************************" << endl;
}

int main() {
  long long int distance = numeric_limits<long long int>::max();
 
  // printMatrix();

  for(int y = 1; y < SIZE; ++y) {
    matrix[0][y] += matrix[0][y-1];
  }

  // printMatrix();

  for(int x = 1; x < SIZE; ++x) {
    matrix[x][0] += matrix[x-1][0];
  }

  // printMatrix() ;

  for(int x = 1; x < SIZE; ++x) {
    for(int y = 1; y < SIZE; ++y) {
      // printMatrix();
      matrix[x][y] += min(getValueAbove(x, y), getValueLeft(x, y));
    }
  }

  printMatrix();

  cerr << "Min: " << matrix[SIZE-1][SIZE-1] << endl;
  return 0;
}
