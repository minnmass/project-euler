#include <vector>
#include <iostream>

using namespace std;

struct frac {
  unsigned long long int num;
  unsigned long long int den;
};

unsigned long long int hcf(int a, int b) {
  if(b == 0) {
    return a;
  }
  return (hcf(b, a%b));
}

void reduce (frac &f) {
  unsigned long long int  h = hcf(f.num, f.den);
  f.num = f.num / h;
  f.den = f.den / h;
}

bool equal(frac f1, frac f2) {
  return ((f1.num * f2.den) == (f1.den * f2.num));
}

bool cmp(frac f1, frac f2) {
  // b < a; == b.operator<(a);
  return ((f1.num * f2.den) < (f1.den * f2.num));
}

void print(frac f) {
  cout << f.num << "/" << f.den;
}

int main() {
  int max = 1000001;
  //  int max = 1000001;
  vector<frac> v;
  
  frac target;
  target.num = 3;
  target.den = 7;
  
  frac f;
  f.num = 0;
  f.den = 1;
  v.push_back(f);
  unsigned long long int minnum = 0;
  unsigned long long int minden = 1;
  unsigned long long int maxnum = 1;
  for(int i = 1; i < max; i++) {
    f.den = i;
    minnum = v[v.size()-1].num;
    maxnum = (3*i)/7;
    for(int j = minnum; j <= maxnum; j++) {
      f.num = j;
      if(cmp(f,target)) {
	v.push_back(f);
      } else {
	j = maxnum + 1;
      }
    }
    sort(v.begin(), v.end(), cmp);
  }
  
  cout << v.size() << " elements" << endl;
  cout << "last is "; print(v[v.size() - 1]);
  cout << " (reduces to ";
  reduce(v[v.size() - 1]);
  print(v[v.size() - 1]); cout << ")" << endl;
  return 0;
}

    /*
    if((i%1000) == 0) {
      if((i%10000) == 0) {
	cout << "den=" << i << endl;
      }
      sort(v.begin(), v.end(), cmp);
      minnum = v[v.size() - 1].num;
      minden = v[v.size() - 1].den;
    }
    f.num = 0;
    f.den = i;
    for(int j = (minnum * i)/minden; (j < max) && (cmp(f, target)); j++) {
      f.num = j;
      v.push_back(f);
    }
  }
  
  cout << "starting sort" << endl;
  sort(v.begin(), v.end(), cmp);
  cout << "sorted " << v.size() << " elements" << endl;

  for(int i = 1; i < v.size(); i++) {
    if(equal(v[i], target)) {
      cout << v[i-1].num << "/" << v[i-1].den << endl;
      reduce(v[i-1]);
      cout << "reduces to: " << v[i-1].num << "/" << v[i-1].den << endl;
      return 0;
    }
  }

  cout << "3/7 not found" << endl;
  cout << "largest was " << v[v.size() - 1].num << "/" << v[v.size() - 1].den << endl;
  reduce (v[v.size() - 1]);
  cout << " - reduces to " << v[v.size() - 1].num << "/" << v[v.size() - 1].den << endl;
  return 0;
}
    */
