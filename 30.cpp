#include <iostream>

using namespace std;

unsigned long long int pow(unsigned long long int base, int exp) {
  unsigned long long int ans = base;

  for(int i = 1; i < exp; i++) {
    ans *= base;
  }
  return ans;
}

bool good(unsigned long long int value, int exp) {
  unsigned long long int sum = 0;
  unsigned long long int original = value;

  while(value > 0) {
    sum += pow(value % 10, exp);
    value /= 10;
  }

  return (sum == original);
}

int main() {
  unsigned long long int max = 0;
  unsigned long long int sum = 0;
  int exp = 5;

  for(int i = 0; i < exp; i++) {
    max += pow(9, exp);
  }

  cout << "max is " << max << endl;

  for(unsigned long long int i = 2; i < max; i++) {
    if(good(i, exp)) {
      cout << i << " works" << endl;
      sum += i;
    }
  }
  
  cout << "...and, the sum is " << sum  << endl;
  return 0;
}
