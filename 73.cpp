#include <vector>
#include <iostream>
#include <math.h>
#include "primes.h"

using namespace std;

struct frac {
  unsigned long long int num;
  unsigned long long int den;
};

void print(frac f) {
  cout << f.num << "/" << f.den;
}

unsigned long long int hcf(int a, int b) {
  if(b == 0) {
    return a;
  }
  return (hcf(b, a%b));
}

void reduce (frac &f) {
  unsigned long long int  h = hcf(f.num, f.den);
  f.num = f.num / h;
  f.den = f.den / h;
}

bool pr(frac f1, frac f2) {
  return ((f1.num * f2.den) == (f1.den * f2.num));
}

int equal(frac f1, frac f2, bool debug = false) {
  if(debug) {
    cout << "comparing "; print(f1); cout << " with "; print(f2); cout << ".\n";
    cout << f1.num * f2.den << "=?=" << f1.den * f2.num << "\n";
  }
  return ((f1.num * f2.den) == (f1.den * f2.num)) ? 0 : 1; // yes, return 1 if they're different
}

bool cmp(frac f1, frac f2, bool debug = false) {
  // b < a; == b.operator<(a);
  if(debug) {
    cout << "comparing "; print(f1); cout << " with "; print(f2); cout << ".\n";
    cout << f1.num * f2.den << " ?< " << f1.den * f2.num << "\n";
  }
  return ((f1.num * f2.den) < (f1.den * f2.num));
}

int main() {
  int max = 10001;
  //  int max = 100001;
  vector<frac> v;

  frac low;
  low.num = 1;
  low.den = 3;
  frac high;
  high.num = 1;
  high.den = 2;

  frac f;
  int answer2 = 0;
  for(int i = 1; i < max; i++) {
    for(int j = (i/3); j < (i/2) + 1; j++) {
      f.num = j;
      f.den = i;
      reduce(f);
      v.push_back(f);
    }
  }
  
  cout << "starting sort" << endl;
  sort(v.begin(), v.end(), cmp);
  cout << "sorted " << v.size() << " elements" << endl;

  int unique = 0;
  int i = 0;
  while(cmp(v[i], low)) {
    i++;
  }
  i++;
  while(cmp(v[i], high)) {
    /*
    if(equal(v[i-1], v[i]) == 0) {
      print(v[i]);
    } else {
      unique++;
      cout << "    ";
      print(v[i]);
    }
    cout << endl;
    */
    unique += equal(v[i-1],v[i]);
    i++;
  }
  cout << unique << " unique ones" << endl;
  return 0;
}
