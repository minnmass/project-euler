#include <iostream>
#include <bitset>

using namespace std;

bool pan(unsigned long long int num) {
  bitset<10> unused;
  unused.set();
  
  //if the number is patently too large or too small, die
  if((num > 987654322) || (num < 123456788)) {
    return false;
  }

  while (num > 0) {
    unused[num % 10] = false;
    num /= 10;
  }
  unused[0] = false;

  return (!unused.any());
}

unsigned long long int  append(unsigned long long int front, unsigned long long int back) {
  if(back < 10) {
    front *= 10;
    front += back;
  } else {
    front = append(front, back / 10);
    front = append(front, back % 10);
  }
  return front;
}

int main() {
  unsigned long long int testval;
  unsigned long long int foo;

  for(int i = 1; i < 987654322; i++) {
    testval = 0;
    for(int j = 1; j < 10; j++) {
      foo = i * j;
      testval = append(testval, foo);
      if(pan(testval)) {
	cout << testval << '\n';
	j = 11;
      } else {
	cerr << testval << '\n';
      }
    }
  }
  return 0;
}
