#include <iostream>
#include <string>

using namespace std;

int main() {
  string number = "                ";
  //               1234567890123456
  bool done = false;

  int i;
  bool carry;
  size_t foundA, found1, found0;
  int found = 0;
  while (! done) {
    carry = true;
    i = 0;
    while (carry && i < number.length()) {
      carry = false;
      switch (number[i]) {
      case '0':
      case ' ':
	number [i] = '1';
	break;
      case '1':
	number [i] = '2';
	break;
      case '2':
	number [i] = '3';
	break;
      case '3':
	number [i] = '4';
	break;
      case '4':
	number [i] = '5';
	break;
      case '5':
	number [i] = '6';
	break;
      case '6':
	number [i] = '7';
	break;
      case '7':
	number [i] = '8';
	break;
      case '8':
	number [i] = '9';
	break;
      case '9':
	number [i] = 'A';
	break;
      case 'A':
	number [i] = 'B';
	break;
      case 'B':
	number [i] = 'C';
	break;
      case 'C':
	number [i] = 'D';
	break;
      case 'D':
	number [i] = 'E';
	break;
      case 'E':
	number [i] = 'F';
	break;
      case 'F':
	number [i] = '0';
	carry = true;
	++i;
	break;
      } // switch
    } // while
    //    cout << number << endl;
    
    foundA = number.find('A');
    if (foundA != string::npos) {
      found1 = number.find('1');
      if (found1 != string::npos) {
	found0 = number.find('0');
	if (found0 != string::npos) {
	  //	  cout << "found a number: " << number << endl;
	  found++;
	}
      }
    }

    if (carry && i == number.length()) {
      done = true;
    }
  }

  cout << "Found a total of " << found << " numbers." << endl;
  cout << "(or, in hex, " << hex << found << ")" << endl;
  return 0;
}

	

    
