#ifndef PANDIGITAL
#define PANDIGITAL

bool isPandigital(unsigned long long int value) {
  // doesn't like one-digit numbers much
  if(value == 1) {
    return true;
  }
  if(value < 10) {
    return false;
  }

  if(value > 987654321) {
    // ... why not ...y
    return false;
  }

  bool found[10];
  found[0] = true;
  for(int i = 1; i < 10; ++i) {
      found[i] = false;
  }
  
  int tmp;
  int digits = 0;
  while(value > 0) {
    ++digits;
    tmp = value % 10;
    if(found[tmp]) {
      return false;
    }
    found[tmp] = true;
    value /= 10;
  }

  for(int i = 1; i < digits; i++) {
    if(!found[i]) {
      return false;
    }
  }
  return true;
}

#endif
