#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

bool isAbundant(unsigned long long int val) {
  unsigned long long int sum = 0;
  for(unsigned long long int i = 1; i < val; i++) {
    if((val % i) == 0) {
      sum += i;
    }
  }

  return (sum > val);
}

int main () {
  unsigned long long int sum = 0;
  unsigned long long int num = 0;

  unsigned long long int max = 28123;
  //unsigned long long int max = 200;

  vector<unsigned long long int> abundant;
  vector<unsigned long long int> sums;

  for(unsigned long long int i = 1; i <= max; i++) {
    if (isAbundant(i)) {
      abundant.push_back(i);
      cout << "pushed " << i << '\n';
    }
  }

  cout << "Done finding abundant numbers." << '\n';

  for(int i = 0; i < abundant.size(); i++) {
    for(int j = 0; j < abundant.size(); j++) {
      sums.push_back(abundant[i] + abundant[j]);
    }
  }

  cout << "Done with sums - found " << sums.size() << '\n';

  sort(sums.begin(), sums.end());

  cout << "Sorted." << '\n';

  for(int i = 0; i <= max; i++) {
    if(!binary_search(sums.begin(), sums.end(), i)) {
      sum += i;
      cout << "sum so far: " << sum << '\n';
    }
  }

  cerr << "The total is " << sum << endl;
  cerr << "We found " << abundant.size() << " abundant numbers." << endl;
  return 0;
}
