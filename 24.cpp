#include <iostream>
#include <string>
#include <algorithm>

using namespace std;

int main() {
  string s = "0123456789";

  for(int i = 0; (i < (1000000 - 1)) && next_permutation(s.begin(), s.end()); i++) {
    // do nothing; the loop will break us out
  }

  cout << s << endl;

  return 0;
}
