#include <iostream>
#include <vector>

#define ulong unsigned long long int

using namespace std;

inline ulong max (ulong a, ulong b) {
  return a > b ? a : b;
}

struct element {
  ulong value;
  ulong sum;
  int row;
};

vector<element> tree;

inline int lChild(int x) { return tree[x].row + x; }

inline int rChild(int x) { return tree[x].row + x + 1; }

int lParent(int x) {
  if(x < 3) {
    return 0; // negative, 0, 1, 2
  }
  int naieve = x - tree[x].row;
  if(tree[naieve].row == tree[x].row - 1) {
    return naieve;
  }
  return 0;
}

int rParent(int x) {
  if(x < 2) {
    return 0; // negative, 0, 1
  }
  int naieve = (x - tree[x].row) + 1;
  if(tree[naieve].row == tree[x].row - 1) {
    return naieve;
  }
  return 0;
}

int main() {
  element tmp;
  tmp.value = 0;
  tmp.sum = 0;
  tmp.row = 0;
  tree.push_back(tmp);

  while(!cin.eof()) {
    for(int i = 0; i < tmp.row; ++i) {
      cin >> tmp.value;
      tree.push_back(tmp);      
    }
    ++tmp.row;
  }

  ulong max_path = 0;
  for(int i = 1; i < tree.size(); i++) {
    tree[i].sum = max(tree[lParent(i)].sum, tree[rParent(i)].sum) + tree[i].value;
    max_path = max(max_path, tree[i].sum);
  }

  for(int i = 0; i < tree.size(); ++i) {
    cout << tree[i].value << "(" << tree[i].sum << ")  " << '\t';
    if(tree[i].row != tree[i+1].row) {
      cout << '\n' << '\n';
    }
  }
  cout << endl;

  cout << "Maximum path: " << max_path << "." << endl;
}
