#ifndef LONGINT
#define LONGINT
// #define LONGING

#include <list>
#include <iostream>
#include <iomanip>
using namespace std;
#define MAX 1000

class longInt {
 private:
  std::list<unsigned int> value;
  void normalize();
  int width;
  void setWidth();
 public:
  void print(std::ostream& os);
  longInt();
  longInt(unsigned int v);
  longInt(std::list<unsigned int> v);
  longInt operator+ (unsigned int v);
  longInt reversed();
  longInt operator+ (longInt v);
  void operator+= (unsigned int v);
  void operator+= (longInt v);
  bool operator== (longInt l) const;
  bool operator> (longInt l);
  bool operator< (longInt l);
  bool operator!= (longInt l) const;
  bool operator== (unsigned int v) const;
  bool operator!= (unsigned int v) const;
  void operator= (unsigned int v);
  void operator= (longInt l);
  void print();
  friend std::ostream& operator<< (std::ostream& os, longInt &i);
};

void longInt::operator= (unsigned int v) {
  value.clear();
  while(v > 0) {
    value.push_back(v % MAX);
    v /= MAX;
  }
}

void longInt::operator= (longInt l) {
  //  value.clear();
  value.assign(l.value.begin(), l.value.end());
  normalize();
}

void longInt::setWidth() {
  width = 0;
  int temp = MAX;
  while(temp > 0) {
    temp /= 10;
    width += 1;
  }
  width--;
}

longInt::longInt() {
  //do nothing
  value.push_back(0);
  setWidth();
}

void longInt::print(std::ostream& os) {
  std::list<unsigned int>::iterator iter = --value.end();
  //  iter = l.value.begin();
  os << *iter;
  while(iter != value.begin()) {
    --iter;
    os << ',' << std::setw(width) << std::setfill('0') << *iter;
  }
}

void longInt::print() {
  cout << this << endl;
}

longInt longInt::reversed() {
  std::list<unsigned int> temp;
  temp.assign(value.begin(), value.end());
  temp.reverse();
  longInt foo(temp);
  std::list<unsigned int>::iterator iter;
  std::list<unsigned int> q;
  for(iter = temp.begin(); iter != temp.end(); ++iter) {
    while(*iter > 0) {
      q.push_back(*iter % 10);
      *iter /= 10;
    }
    while(!q.empty()) {
      *iter *= 10;
      *iter += q.front();
      q.pop_front();
    }
  }
  longInt ret(temp);
  return ret;
}
    
/*  longInt ret;
  list<unsigned int> temp;
  temp.assign(value.begin(), value.end());
  std::list<unsigned int>::iterator iter = --temp.end();
  while(iter != temp.begin()) {
    while(*iter > 0) {
      ret += *iter % 10;
      *iter /= 10;
    }
    --iter;
  }
  return ret; 
  } */
	

bool longInt::operator== (unsigned int v) const {
  longInt foo(v);
  return value == foo.value;
}

bool longInt::operator!= (unsigned int v) const {
  longInt foo(v);
  return value != foo.value;
}

inline bool longInt::operator> (longInt l) {
  if(value == l.value) {
    return false;
  }
  std::list<unsigned int>::iterator itera = --value.end();
  std::list<unsigned int>::iterator iterb = --l.value.end();

  while((itera != value.begin()) && (iterb != l.value.begin())) {
    if(*itera < *iterb) {
      return false;
    }
    --itera;
    --iterb;
  }

  // if a is longer, then iterb will break the loop
  return (iterb == l.value.begin());
}

inline bool longInt::operator< (longInt l) {
  if(value == l.value) {
    return false;
  }
  std::list<unsigned int>::iterator itera = --value.end();
  std::list<unsigned int>::iterator iterb = --l.value.end();

  while((itera != value.begin()) && (iterb != l.value.begin())) {
    if(*itera > *iterb) {
      return false;
    }
    --itera;
    --iterb;
  }

  // if a is longer, then iterb will break the loop
  return (iterb != l.value.begin());
}

inline bool longInt::operator!= (longInt l) const {
  return (value != l.value);
}

inline bool longInt::operator== (longInt l) const {
  return (value == l.value);
}

longInt longInt::operator+ (unsigned int v) {
  longInt ret(value);
  ret += v;
  return ret;
}

longInt longInt::operator+ (longInt v) {
  longInt ret(value);
  ret += v;
  return ret;
}

void longInt::operator+= (unsigned int v) {
  std::list<unsigned int>::iterator iter = value.begin();
  *iter += v;
  normalize();
}

void longInt::operator+= (longInt v) {
  std::list<unsigned int>::iterator itera;
  std::list<unsigned int>::iterator iterb;
  
  itera = value.begin();
  iterb = v.value.begin();
  
  while((itera != value.end()) && (iterb != v.value.end())) {
    *itera += *iterb;
    ++itera;
    ++iterb;
  }
  
  while(iterb != v.value.end()) {
    value.push_front(*iterb);
    ++iterb;
  }
  
  normalize();
}

std::ostream& operator<< (std::ostream& os, longInt& l) {
  l.print(os);
  return os;
}

longInt::longInt(unsigned int v) {
  while(v > 0) {
    value.push_back(v % MAX);
    v /= MAX;
  }
  setWidth();
}

longInt::longInt(std::list<unsigned int> v) {
  value = v;
  normalize();
  setWidth();
}

void longInt::normalize() {
  std::list<unsigned int>::iterator iter;
  iter = value.begin();
  unsigned int overflow = 0;

  while(iter != value.end()) {
    *iter += overflow;
    overflow = 0;
    while(*iter > MAX) {
      ++overflow;
      *iter /= MAX;
    }
    ++iter;
  }
  while(overflow > 0) {
    value.push_back(overflow % MAX);
    overflow /= MAX;
  }
  if(*(--value.end()) == 0) {
    value.pop_back();
  }
}

#endif
