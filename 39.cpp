#include <iostream>
#include <math.h>

using namespace std;

int main() {
  int p[1001];
  for(int i = 0; i < 1001; i++) {
    p[i] = 0;
  }


  unsigned long long int a;
  unsigned long long int b;
  unsigned long long int c;
  unsigned long long int tmp;
  for(unsigned long long int i = 3; i < 1001; i++) {
    for(a = 1; a < i; a++) {
      for(b = a; b < (i - a); b++) {
	tmp = (a*a) + (b*b);
	c = sqrt(tmp);
	if((c*c) == tmp) { // check for "rounding that works, but shouldn't"
	  if((a + b + c) == i) {
	    p[i]++;
	  }
	}
      }
    }
  }

  int max = 0;
  for(int i = 0; i < 1001; i++) {
    if(p[max] < p[i]) {
      max = i;
    }
  }

  cout << max << "(with " << p[max] << ")" << endl;
  return 0;
}
