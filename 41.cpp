#include <iostream>
#include "primes.h"
#include <string>
#include <algorithm>
#include <vector>

using namespace std;

int toInt(string s) {
  int x = 0;
  for(int i = 0; i < s.size(); i++) {
    x *= 10;
    x += s[i] - '0';
  }
  return x;
}

string toString(int x) {
  string s;
  char c;
  while(x > 0) {
    c = (x % 10) + '0';
    s = c + s;
    x /= 10;
  }
  return s;
}

string next(string s) {
  string ret = s.substr(1);
  ret += s[0];
  return ret;
}

bool isPanDigital(unsigned long long int n) {
  string s = toString(n);
  sort(s.begin(), s.end());
  for(int i = 1; i <= s.size(); i++) {
    if(s[i-1] != (i + '0')) {
      return false;
    }
  }
  return true;
}

int main() {
  vector<unsigned long long int> v;
  unsigned long long int max = 1000000000;
  cout << max << '\n';
  cout << "123456789" << '\n';
  prime p(max);
  cout << "done making p" << '\n';
  cout << "largest = " << p.at(p.size() - 1) << endl;
  cout << "(size=" << p.size() << ")" << endl;

  for(int i = p.size(); i > 0; i--) {
    if(isPanDigital(p.at(i))) {
      cout << p.at(i) << endl;
    }
  }

  return 0;
}

  
