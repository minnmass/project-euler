#include <iostream>
#include <vector>
#include <math.h>

using namespace std;

int main() {
  vector<int> primes;

  primes.push_back(2);
  primes.push_back(3);

  bool possible;
  for(int i = 4; i < 1000000; i++) {
    //  for(int i = 4; i < 10; i++) {
    possible = true;
    for(int j = 0; (j < sqrt(primes.size())) && possible; j++) {
      if(!(i%primes[j])) {
	possible = false;
      }
    }
    if(possible) {
      primes.push_back(i);
    }
  }

  unsigned long long sum = 0;
  for(int i = 0; i < primes.size(); i++) {
    sum += primes[i];
  }

  cout << "sum of " << primes.size() << "= " << sum << endl;
  return 0;
}
