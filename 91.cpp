#include <iostream>
#include <cmath>
using namespace std;

#define MAX 50

inline long double distance(int x1, int y1, int x2, int y2) {
  return sqrt((long double)pow(x2 - x1, 2) + (long double)pow((y2 - y1), 2));
}

inline long double max(long double a, long double b) {
  return a > b ? a : b;
}

inline long double min(long double a, long double b) {
  return a < b ? a : b;
}

inline long double biggest(long double a, long double b, long double c) {
  return max(a, max(b, c));
}

inline long double smallest(long double a, long double b, long double c) {
  return min(a, min(b, c));
}

long double middle(long double a, long double b, long double c) {
  long double big = biggest(a, b, c);
  long double small = smallest(a, b, c);

  if((a != big) && (a != small)) {
    return a;
  }
  if((b != big) && (b != small)) {
    return b;
  }
  return c; // default; used if two numbers are equal
}

int main() {
  long double a, b, c;
  long double big, small, mid;

  unsigned int count = 0;

  for(int x1 = 0; x1 <= MAX; ++x1) {
    for(int x2 = 0; x2 <= MAX; ++x2) {
      for(int y1 = 0; y1 <= MAX; ++y1) {
	for(int y2 = 0; y2 <= MAX; ++y2) {
	  a = distance(0, 0, x1, y1);
	  b = distance(0, 0, x2, y2);
	  c = distance(x1, y1, x2, y2);
	  big = biggest(a, b, c);
	  small = smallest(a, b, c);
	  mid = middle(a, b, c);
	  
	  if(pow(big, 2) == (pow(small, 2) + pow(mid, 2))) {
	    ++count;
	    cout << "(" << x1 << "," << y1 << "),(" << x2 << "," << y2 << ")\n";
	  }
	}
      }
    }
  }

  count -= 1; // counts (0,0),(0,0),(0,0), which it shouldn't

  cerr << "Found " << count << " triangles twice." << endl;
  cerr << "So, " << count/2 << " triangles total." << endl;

  return 0;
}
