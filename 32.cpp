#include <iostream>

#include "pandigital.h"

#include <set>

using namespace std;

// number of digits; '-' doesn't count
unsigned int numDigits(unsigned long long int x) {
  int digits = 0;
  while(x > 0) {
    x /= 10;
    ++digits;
  }
  return digits;
}

// creates a combined int with all of the digits of the two numbers
// order is irrelivent
unsigned long long int combine(unsigned long long int a, unsigned long long int b) {
  if(b == 0) {
    return a * 10;
  }

  while (b > 0) {
    a *= 10;
    a += b % 10;
    b /= 10;
  }
  return a;
}

int main() {
  set<unsigned int> products;

  int max = 9876543; // 2 digits (at least) will be provided by i and j
  unsigned long long int product;
  unsigned long long int bigNum;

  bigNum = 0;
  --bigNum;
  cout << "max bigNum: " << bigNum << endl;

  for(int i = 1; i < max; i++) {
    for(int j = i; j < max; j++) {
      product = i * j;
      if(product > 987654321) {
	break;
      }
      bigNum = combine(combine(i, j), product);
      if(bigNum > 987654321) {
	continue;
      }
      if((numDigits(bigNum) == 9) && (isPandigital(bigNum))) {
	products.insert(product);
	cerr << i << " * " << j << " = " << product << " -> " << bigNum << '\n';
      }
      // cout << i << " * " << j << " = " << product << " -> " << bigNum << '\n';
      // cout << "numDigits: " << numDigits(bigNum) << " and isPandigital: " << isPandigital(bigNum) << '\n';
      // cout << '\n';
    }
    cout << "done with i = " << i << endl;
  }

  cout << "Found " << products.size() << " pandigital-generating products." << endl;
  return 0;
}
	
	  
