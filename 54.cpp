#include <iostream>
#include "hand.h"

using namespace std;

int main() {
  hand h1("8C TS KC 9H 4S");
  hand h2("2C 3S 8S 8D TD");
  cout << h1 << "(" << h1.value() << ") ";
  if(h1 > h2) {
    cout << ">";
  } else {
    cout << "<";
  }
  cout << h2 << "(" << h2.value() << ")" << endl;

  return 0;
}
