#include <iostream>

using namespace std;

unsigned long long int reverse(unsigned long long int x) {
  unsigned long long int ret = 0;
  while(x > 0) {
    ret *= 10;
    ret += x % 10;
    x /= 10;
  }
  return ret;
}

bool allOdd(unsigned long long int x) {
  while(x > 0) {
    if(((x % 10) % 2) == 0) {
      return false;
    } else {
      x /= 10;
    }
  }
  return true;
}

int main() {
  unsigned long long int max = 100000000;
  unsigned long long int reversible = 0;
  unsigned long long int rev;
  int test = 0;

  for(unsigned long long int i = 1; i < max; i++) {
    rev = reverse(i);
    if(allOdd(i + rev)/* && (i == reverse(rev))*/) {
      reversible++;
      if(i != reverse(rev)) {
	reversible--;
	test++;
      }
    }
  }

  cout << reversible << " reversible numbers found." << endl;
  cout << "(2m25.575s)" << endl;
  cout << test << " failed i != rev(rev(i))" << endl;
  return 0;
}
