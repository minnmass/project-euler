#include <iostream>

using namespace std;

unsigned long long int fact(unsigned long long int x) {
  if(x <= 1) {
    return 1;
  }
  return x * fact(x-1);
}

bool good(unsigned long long int value) {
  unsigned long long int sum = 0;
  unsigned long long int original = value;

  while(value > 0) {
    sum += fact(value % 10);
    value /= 10;
  }

  return (sum == original);
}

int main() {
  unsigned long long int max = 9999999;
  unsigned long long int sum = 0;

  max = 7* fact(9);

  cout << "max is " << max << endl;

  for(unsigned long long int i = 3; i < max; i++) {
    if(good(i)) {
      cout << i << " works" << endl;
      sum += i;
    }
  }
  
  cout << "...and, the sum is " << sum  << endl;
  return 0;
}
