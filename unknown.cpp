#include <iostream>
using namespace std;

#define MAX 20

bool incriment(int array[]) {
  bool done = false;
  int index = MAX - 1;

  while((!done) && index >= 0) {
    array[index]++;
    if(array[index] <= 9) {
      done = true;
    } else {
      array[index] = 0;
      index--;
    }
  }
  return done;
}

void print(int array[]) {
  for(int i = 0; i < MAX; i++) {
    cout << array[i];
  }
  return;
}

int main() {
  int array[MAX];
  unsigned long long int answer = 0;
  bool done;

  array[0] = 1;
  for(int i = 1; i < MAX; i++) {
    array[i] = 0;
  }

  while(incriment(array)) {
    done = false;
    for(int i = 0; (i < MAX - 2) && !done; i++) {
      if(array[i] + array[i+1] + array[i+2] > 9) {
	done = true;
      }
    }
    if(!done) {
      answer++;
    }
  }

  cout << "found " << answer << " numbers." << endl;

  return 0;
}

