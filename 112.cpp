#include <iostream>

#define TARGET 100
// remember we want target percentage * 10
// eg. 100 = 10%

using namespace std;

bool isBouncy(unsigned long long int b) {
  cout << "checking " << b << endl;
  if(b < 100) {
    return false;
  }
  int current;
  int previous;
  bool decending = false;
  
  previous = b % 10;
  b /= 10;
  current = b % 10;
  if(previous > current) {
    decending = true;
  }
  while(b > 0) {
    previous = current;
    b /= 10;
    current = b % 10;
    if(decending) {
      if (previous < current) {
	cout << previous << " < " << current << endl;
	return true;
      }
    } else {
      if (previous > current) {
	cout << previous << " > " << current << endl;
	return true;
      }
    }
  }
  return false;
}

int main() {
  if (isBouncy(134468)) {
    cout << "134468 is bouncy" << endl;
  }
  if (isBouncy(66420)) {
    cout << "66420 is bouncy" << endl;
  }
  if (isBouncy(155349)) {
    cout << "155349 is bouncy" << endl;
  }
  return 0;
}

/*

int main() {
  unsigned long long int bouncy = 0;
  unsigned long long int considered = 0;

  int i = 1;
  while((10 * bouncy) / (10 * considered) != TARGET) {
    // while we haven't reached 99%...
    ++considered;
    if (isBouncy(i)) {
      ++bouncy;
    }
  }

  cout << "found " << TARGET / 10 << "% at " << i << endl;
  return 0;
}

/* */
