#include <iostream>
#include <iomanip>

using namespace std;

int main() {
  int size = 1001;
  int grid[size][size];
  for(int i = 0; i < size; i++) {
    for(int j = 0; j < size; j++) {
      grid[i][j] = 0;
    }
  }

  int mid = size/2;
  
  int next = 1;

  int i = mid;
  int j = mid;
  int max = size*size;
  bool changej = true;
  int offset = 1;
  int moves = 0;
  int last_moves = 1;
  for(; next <= max; next++) {
    grid[i][j] = next;
    moves++;
    if(changej) {
      j += offset;
    } else {
      i += offset;
    }
    if(moves == last_moves) {
      changej = !changej;
      if(changej) {
	offset = -offset;
	last_moves++;
      }
      moves = 0;
    }
  }

  /*
  for(int i = 0; i < size; i++) {
    for(int j = 0; j < size; j++) {
      cout << setw(5) <<  grid[i][j] << " ";
    }
    cout << endl;
  }
  */

  unsigned long long int sum = 0;
  for(int i = 0; i < size; i++) {
    sum += grid[i][i];
    sum += grid[i][size-(i+1)];
  }

  cout << "sum: " << sum << endl;
  cout << "or:  " << sum-1 << " if you don't count the 1 twice" << endl;
  return 0;
}
  
