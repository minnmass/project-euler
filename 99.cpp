#include <iostream>
#include <vector>
#include <fstream>

using namespace std;

int main() {
  vector<pair<unsigned long long int, unsigned long long int> > v;
  pair<unsigned long long int, unsigned long long int> p;

  vector<int> possible;

  ifstream ins("99.input");
  if(!ins) {
    cout << "Error opening input file; bailing." << endl;
    exit(-1);
  }

  while (!ins.eof()) {
    ins >> p.first; ins >> p.second;
    v.push_back(p);
  }

  bool small = false;
  for(int i = 0; i < v.size(); i++) {
    small = false; // we don't know that this is smaller than something else
    for(int j = (i + 1); (!small) && (j < v.size()); j++) {
      if(!((v[i].first > v[j].first) && (v[i].second > v[i].second))) {
	small = true;
      }
    }
    if(!small) {
      possible.push_back(i);
    }
  }

  cout << v.size() << endl;
  cout << possible.size() << endl;
  cout << (possible.size()/(float)v.size()) * 100 << "%" << endl;
  cout << possible[0] << " = " << v[possible[0]].first << "^" << v[possible[0]].second << endl;
  return 0;
}
