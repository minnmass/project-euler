#include <iostream>
#include <vector>

#include "primes.h"

using namespace std;

int main() {
  cout << "starting" << endl;
  prime p(10);

  int upto = 1000;

  long long int product;
  unsigned long long int max = 0;
  unsigned long long int num;
  unsigned long long int n;
  long long int test;
  for(int a = -upto; a <= upto; a++) {
    for(int b = 0; b <= upto; b++) {
      num = 0;
      n = 0;
      test = (n*n) + (a*n) + b;
      while ((test > 0) && p.isPrime(test, false)) {
	n++;
	num++;
	test = (n*n) + (a*n) + b;
      }
      if(num > max) {
	cout << "setting max to " << num << '\n';
	cout << "a = " << a << ", b = " << b;
	max = num;
	product = a * b;
	cout << ", product = " << product << endl;
      }
    }
  }

  //  p.print();
  cout << max << " at a*b=" << product << endl;
  return 0;
}
