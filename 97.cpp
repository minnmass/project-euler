#include <iostream>
#include <iomanip>

using namespace std;

int main() {
  unsigned long long int tot = 1;
  unsigned long long int foo = 10000000000;
  unsigned long long int bar;
  cout << setw(10);
  for(int i = 0; i < 78; i++) {
    tot = (tot*2);
    bar = tot % foo;
    cout << bar << endl;
  }

  return 0;
}
