#include <iostream>
#include <vector>

using namespace std;

int main () {
  vector<unsigned int> v;

  v.push_back(0);
  v.push_back(1);
  v.push_back(1);

  unsigned int sum = 0;

  cout << v.size() << endl;

  while (sum < 1000000) { // less than 1 million
    cout << sum << " ";
    sum = v[v.size() - 1] + v[v.size() - 2];
    v.push_back(sum);
  }

  sum = 0;

  for(int i = 0; i < v.size(); i++) {
    if(!(v[i] % 2)) {
      sum += v[i];
    }
  }

  cout << sum << endl;

  return 0;
}
