#include <iostream>

using namespace std;
#define uint unsigned long long int

uint pow(uint x, uint y) {
  uint result = 1;
  for(int i = 0; i < y; ++i) {
    result *= x;
  }
  return result;
}

uint numDigits(uint x) {
  uint digits = 0;
  while(x > 0) {
    x /= 10;
    ++digits;
  }
  return digits;
}

int main() {
  int length = 0;
  uint result;
  int answer = 0;
  uint power;

  answer = pow(7, 5);
  cout << "7^5 = " << answer << " -> " << numDigits(answer) << endl;
  return 0;
  

  for(uint base = 1; base <= 7; ++base) {
    power = 2;
    do {
      result = pow(base, power);
      length = numDigits(result);
      if(length == power) {
	cout << base << "^" << power << " = " << result << endl;
	++answer;
	break;
      }
    } while (length < power);
  }
    
  cout << "Found " << answer << " answers." << endl;
  return 0;
}
