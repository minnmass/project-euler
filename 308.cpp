#include <iostream>
#include <vector>
using namespace std;
#define uint unsigned long long int

struct fval {
  uint num;
  uint den;
};

uint factran (uint val, vector<fval> fvals) {
  uint num;
  for(int i = 0; i < fvals.size(); ++i) {
    num = val * fvals[i].num;
    if((num % fvals[i].den) == 0) {
      return num / fvals[i].den;
    }
  }
}

void print(vector<fval> fvals) {
  for(int i = 0; i < fvals.size(); ++i) {
    cout << fvals[i].num << " / " << fvals[i].den << '\n';
  }
}

int main() {
  vector<fval> fvals;
  fval f;
  f.num = 17; f.den = 91; fvals.push_back(f);
  f.num = 78; f.den = 85; fvals.push_back(f);
  f.num = 19; f.den = 51; fvals.push_back(f);
  f.num = 23; f.den = 38; fvals.push_back(f);
  f.num = 29; f.den = 33; fvals.push_back(f);
  f.num = 77; f.den = 29; fvals.push_back(f);
  f.num = 95; f.den = 23; fvals.push_back(f);
  f.num = 77; f.den = 19; fvals.push_back(f);
  f.num = 1 ; f.den = 17; fvals.push_back(f);
  f.num = 11; f.den = 13; fvals.push_back(f);
  f.num = 13; f.den = 11; fvals.push_back(f);
  f.num = 15; f.den = 2 ; fvals.push_back(f);
  f.num = 1 ; f.den = 7 ; fvals.push_back(f);
  f.num = 55; f.den = 1 ; fvals.push_back(f);

  uint facval = 2;
  uint iter = 0;
  while(facval != 32) {
    facval = factran(facval, fvals);
    cout << facval << ", ";
    ++iter;
  }
  cout << "..." << endl;
  cout << "took " << iter << " iterations." << endl;
}
