#include <iostream>
#include "longInt.h"

using namespace std;

int main() {
  cout << "a" << endl;
  longInt a;
  longInt d;
  //  for(unsigned int i = 99999; i < 100001; i++) {
  for(unsigned int i = 123456; i < 123459; i++) {
    a = i;
    d = a.reversed();
    if(d > a) {
      cout << "true for ";
    } else {
      cout << "false for ";
    }
    cout << d << " > ";
    cout << a;
    cout << endl;
  }
  return 0;
  cout << "b" << endl;
  longInt b;
  cout << "c" << endl;
  longInt c;

  cout << "setting a" << endl;
  a = 100;
  cout << "a = " << a << endl;
  cout << "setting b" << endl;
  b = a;
  cout << "b = " << b << endl;
  cout << "setting c" << endl;
  c = a + b;
  cout << "c = " << c << endl;

  cout << "incrimenting c by c " << endl;
  c += c;
  cout << c << endl;

  return 0;
}
