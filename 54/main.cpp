#include <iostream>
#include <string>
#include <fstream>
#include "hand.h"
#include "card.h"

using namespace std;

int main() {
  hand left, right;
  ifstream ins;

  ins.open("poker.txt");
  if(!ins) {
    cout << "error w/ poker" << endl;
    return (-1);
  }

  cout << "starting reading" << endl;

  string s;
  int tot = 0;
  getline(ins, s);
  do {
    left.c[0].set(s[0], s[1]);
    left.c[1].set(s[3], s[4]);
    left.c[2].set(s[6], s[7]);
    left.c[3].set(s[9], s[10]);
    left.c[4].set(s[12], s[13]);
    
    right.c[0].set(s[15], s[16]);
    right.c[1].set(s[18], s[19]);
    right.c[2].set(s[21], s[22]);
    right.c[3].set(s[24], s[25]);
    right.c[4].set(s[27], s[28]);
    
    if(left.better(right)) {
      tot++;
    }
    getline(ins, s);
  } while(!ins.eof());
  cout << tot << ", " << bad << endl;
  return 0;
}
