#ifndef CARD_H
#define CARD_H

class card {
 public:
  int value;
  char suit;
  void set (int v, char s);
  void set(char v, char s);
};

void card::set(int v, char s) {
  value = v;
  suit = s;
}

void card::set(char v, char s) {
  switch(v) {
  case 'A':
    value = 1;
    break;
  case 'J':
    value = 11;
    break;
  case 'Q':
    value = 12;
    break;
  case 'K':
    value = 13;
    break;
  default:
    value = v - '0';
  }
  suit = s;
}

#endif
