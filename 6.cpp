#include <iostream>

using namespace std;

unsigned int square(unsigned int x) {
  return x*x;
}

int main() {
  unsigned int sum = 0;
  unsigned int squ = 0;

  for(int i = 1; i <= 100; i++) {
    sum += square(i);
    squ += i;
  }

  cout << square(squ) - sum << endl;
  return 0;
}
