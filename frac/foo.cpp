#include <iostream>
#include <list>

#define MAX 10001

using namespace std;

class fraction {
public:
  int num;
  int den;
  bool operator< (fraction f) {
    return ((num * f.den) < (den * f.num));
  }    
  bool operator== (fraction f) {
    return ((num == f.num) && (den == f.den));
  }
  void print() {
    cout << num << "/" << den;
  };
  fraction(int n, int d) {
    num = n;
    den = d;
  };
};

int gcd(int a, int b) {
  int temp;
  while(b != 0) {
    temp = b;
    b = a % b;
    a = temp;
  }
  return a;
}

int main() {
  list<fraction> l;
  int n;
  int d;
  int g;
  bool used[MAX + 1];
  for(int i = 0; i < MAX; i++) {
    used[i] = false;
  }
  bool possible;
  int num;
  int den;

  for(d = 1; d < MAX; d++) {
    possible = true;
    for(int i = d; i < MAX; i++) {
      if((used[i]) && ((i % d) == 0)) {
	possible = false;
	i = MAX + 1;
      }
    }
    if(possible) {
      for(n = 1; n < d; n++) {
	g = gcd(n, d);
	num = n/g;
	den = d/g;
	used[den] = true;
	fraction f(num, den);
	l.push_back(f);
      }
    }
  }    

  l.sort();
  l.unique();

  /*
  list<fraction>::iterator iter;
  for(iter = l.begin(); iter != l.end(); ++iter) {
    fraction f = *iter;
    f.print();
    cout << endl;
  }
  */
  cout << l.size() << " fractions found" << endl;

  return 0;
}
