#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

inline int value(char c) {
  return (c - 'A') + 1;
}

int main() {
  string temp;
  ifstream ins;
  vector<string> v;
  unsigned long long total = 0;
  int t;

  ins.open("22names.txt");
  if(!ins) {
    cout << "error opening file" << endl;
    exit(-1);
  }

  while(getline(ins, temp)) {
    v.push_back(temp);
  }

  sort(v.begin(), v.end());
  
  for(int i = 0; i < v.size(); i++) {
    t = 0;
    for(int j = 0; j < v[i].size(); j++) {
      t += value(v[i][j]);
    }
    total += t * (i + 1);
  }

  cout << total << endl;
  return 0;
}
