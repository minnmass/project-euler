#include <list>
#include <iostream>
#include "longInt.h"

using namespace std;

bool palendrome(longInt x) {
  return (x == reverse(x));
}


int main() {
  longInt x;
  int index;
  int iterations;
  int lychrel = 0;
  int max = 200;

  for (index = 0; index < max; index++) {
    x = index;
    iterations = 0;
    while((!palendrome(x)) && (iterations <= 51)) {
      iterations++;
      x += reverse(x);
    }
    if(iterations > 50) {
      cout << index << " appears to be lychrel; got " << x << endl;
      lychrel++;
    }
  }

  cout << lychrel << " lychrell numbers found." << endl;
  return 0;
}
