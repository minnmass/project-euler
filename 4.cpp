#include <iostream>
#include <vector>
#include <algorithm>
#include <string>

using namespace std;

bool palindrome(int n);

int main () {
  vector<int> v;

  for(int i = 0; i < 1000; i++) {
    for(int j = 0; j < 1000; j++) {
      v.push_back(i*j);
    }
  }

  sort(v.begin(), v.end());

  int last = 0;
  for(int i = v.size() - 1; i > 0; i--) {
    if(last != v[i]) {
      if(palindrome(v[i])) {
	cout << v[i] << endl;
	return 0;
      }
    }
    last = v[i];
  }

  cout << "none found" << endl;
}

bool palindrome(int n) {
  string s = "";
  int cur = n;
  char c;

  while(cur > 0) {
    c = '0' + (cur%10);
    s = c + s;
    cur /= 10;
  }

  for(int i = 0; i < s.size(); i++) {
    if(s[i] != s[s.size()-(i+1)]) {
      return false;
    }
  }

  return true;
}
