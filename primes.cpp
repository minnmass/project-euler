#include <iostream>
#include "primes.h"

using namespace std;

int main() {
  unsigned long long int max = 0;
  --max;
  prime p(max);
  cout << p.size() << endl;
  for(int i = 0; i < p.size(); ++i) {
    cout << p.at(i) << '\n';
  }
}
