#include <iostream>
#include <iostream>
#include <stack>
using namespace std;

//note: using a temp string rather than a stack takes twice as long...

int main() {
  string s = "";
  stack<char> st;
  int i = 1;
  int x;
  while(s.length() < 1000000) {
    x = i;
    while(x > 0) {
      st.push((x%10) + '0');
      x/=10;
    }
    while(!st.empty()) {
      s.push_back(st.top());
      st.pop();
    }
    ++i;
  }

  unsigned long long prod = 1;
  cout << "starting last for loop" << endl;
  for(i = 1; i <= 1000000; i *= 10) {
    cout << "s[" << i-1 << "]=" << s[i-1] + '0';
    prod *= s[i-1] - '0';
  }
  cout << endl;
  cout << prod << endl;
  return 0;
}
