#include <vector>
#include <iostream>

using namespace std;

int main() {
  vector<int> amicable;

  amicable.push_back(0);
  int max;
  int sum;
  for(int i = 1; i <= 10000; i++) {
    max = i/2;
    sum = 0;
    for(int j = 1; j < max; j++) {
      if((i % j) == 0) {
	sum += j;
      }
    }
    amicable.push_back(sum);
  }

  sum = 0;
  for(int i = 1; i <= 10000; i++) {
    for(int j = i; j <= 10000; j++) {
      if(amicable[i] == j) {
	sum += i + j;
	cerr << i << "+" << j << "+";
      }
    }
  }
  cerr << 0 << endl;;
  cout << sum << endl;
  return 0;
}
