#ifndef PRIMES_H
#define PRIMES_H

#include <cstdlib>
#include <vector>
#include <math.h>

using namespace std;

class prime {
 private:
  vector<unsigned long long> primes;
  unsigned long long last_checked;
 public:
  void add_to(unsigned long long p);
  unsigned long long at(int idx);
  int size();
  bool add();
  prime(unsigned long long p = 1000000);
  bool isPrime(unsigned long long int x);
  void print();
};

void prime::print() {
  for(int i = 0; i < primes.size(); i++) {
    cout << i << ": " << primes[i] << endl;
  }
}

bool prime::isPrime(unsigned long long x) {
#ifdef DEBUG
    cout << "in isPrime(" << x << ")" << endl;
#endif
  add_to(x);
  add();
#ifdef DEBUG
    cout << "done adding up to " << x << endl;
    cout << "there are now " << primes.size() << " primes";
    cout << ", ending with " << primes[primes.size() - 1] << endl;
#endif
  while(primes[primes.size() - 1] < x) {
    if(!add()) {
      cerr << "error adding in isPrime()" << endl;
      cerr << "size=" << size() << endl;
      cerr << "last value=" << primes[primes.size() - 1] << endl;
      exit (-1);
    } else {
      cerr << "added with add() in isPrime()" << endl;
    }
  }

  //do binary search; we get thousands of primes fairly quickly...
  int first = 0;
  int last = primes.size();
  int mid;
  while(first < last) {
    mid = (first + last) / 2;
    if(primes[mid] == x) {
      return true;
    } else if(primes[mid] < x) {
      first = mid + 1;
    } else {
      last = mid;
    }
  }
  return (primes[mid] == x);
}

bool prime::add() {
  int s = primes.size();
  while (s == primes.size()) {
    if((last_checked + 1) == 0) {
      return 0;
    }
    add_to(last_checked + 1);
  }
  return true;
}

prime::prime(unsigned long long p) {
  last_checked = 10;
  primes.push_back(2);
  primes.push_back(3);
  primes.push_back(5);
  primes.push_back(7);
  add_to(p);
}

void prime::add_to(unsigned long long p) {
#ifdef DEBUG
    cout << "in add_to(" << p << "), with " << primes.size() << " primes; last_checked = " << last_checked << endl;
    for(int i = 0; i < primes.size(); i++) {
      cout << i << ": " << primes[i] << endl;
    }
#endif
  bool possible;
  for(unsigned long long int i = last_checked + 1; i <= p; i++) {
    possible = true;
    int maxToCheck = sqrt(i);
    for(unsigned long long int j = 0; possible && (primes[j] <= maxToCheck); j++) {
      if((i % primes[j]) == 0) {
	possible = false;
      }
    }
    if(possible) {
#ifdef DEBUG      
      if((size() % 10000) == 0) {
	cerr << "pushing " << i << " onto primes" << endl;
      }
#endif
      primes.push_back(i);
      last_checked = i;
    }
  }
  if(p > last_checked) {
    last_checked = p;
  }
  return;
}

int prime::size() {
  return primes.size();
}

unsigned long long prime::at(int idx) {
  if(idx >= primes.size()) {
    return 0;
  }
  return primes[idx];
}

#endif
