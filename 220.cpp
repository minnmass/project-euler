#include <iostream>
#include <string>
#include "longInt.h"

#define DN 50
// #define STEPS 1000000000000

using namespace std;

int main() {
  longInt STEPS = 1000000000000;

  // build D[n];
  string D = "Fa";
  string dLast = D;
  for (int i = 0; i < DN; i++) {
    if (!(i % 5)) {
      cout << "running D, with i = " << i << endl;
      cout << "  dLast.length = " << dLast.length() << endl;
    }
    dLast = D;
    D = "";
    for (int j = 0; j < dLast.length(); j++) {
      switch (dLast[j]) {
      case 'a':
	D += "aRbFR";
	break;
      case 'b':
	D += "LFaLb";
	break;
      default:
	D += dLast[j];
      }
    }
  }
  cout << D << endl;

  cout << "Have D, checking for compliance" << endl;

  //reality check: D is long enough to run STEPS steps
  longInt Fs = 0;
  for (longInt i = 0; i < D.length(); i++) {
    if (D[i] == 'F') {
      ++Fs;
    }
  }
  cout << "STEPS = " << STEPS << endl;
  cout << "Fs = " << Fs << endl;
  cout << "D.length() = " << D.length() << endl;
  if (STEPS >= Fs) {
    cout << "Error: trying to run too many steps." << endl;
    return -1;
  }
  // run D
  int x = 0;
  int y = 0;
  char direction = 'n';
  char newdirection;
  longInt i = 0;
  longInt taken = 0;
  while (taken < STEPS) {
    switch (D[i]) {
    case 'F':
      if (direction == 'n') {
	++y;
      } else if (direction == 's') {
	--y;
      } else if (direction == 'e') {
	++x;
      } else if (direction == 'w') {
	--x;
      }
      if (!(taken % 100)) {
	cout << "Just took our " << taken << "th step" << endl;
      }
      ++taken;
      break; // end of "Forward"
    case 'L':
      switch (direction) {
      case 'n':
	newdirection = 'w';
	break;
      case 's':
	newdirection = 'e';
	break;
      case 'e':
	newdirection = 'n';
	break;
      case 'w':
	newdirection = 's';
	break;
      }
      direction = newdirection;
      break; // end of "turn Left"
    case 'R':
      switch (direction) {
	case 'n':
	newdirection = 'e';
	break;
      case 's':
	newdirection = 'w';
	break;
      case 'e':
	newdirection = 's';
	break;
      case 'w':
	newdirection = 'n';
	break;
      }
      direction = newdirection;
      break; // end of "turn Right"
    default:
      // this is an 'a' or 'b'
      // and should be ignored
      break;
    }
    ++i; // next character, silly
  } // end of for loop - D has been run

  cout << "x = " << x << endl;
  cout << "y = " << y << endl;
  cout << "Facing " << direction << endl;

  return 0;
}
