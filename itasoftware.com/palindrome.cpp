// big assumption: all letters are lower-case

#define ALPHABET_LENGTH 26

#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <algorithm>

using namespace std;

// vector<string> alphabet ("Aa","Bb","Cc","Dd","Ee","Ff","Gg","Hh","Ii","Jj","Kk","Ll","Mm","Nn","Oo","Pp","Qq","Rr","Ss","Tt","Uu","Vv","Ww","Xx","Yy","Zz");

bool isPalindrome (string S) {
  int front = 0;
  int back = S.length() - 1; // length = # of elements; we want the index

  while (front < back) {
    while (S[front] == ' ') {
      ++front;
    }
    while (S[back] == ' ') {
      --back;
    }
    if (S[front] != S[back]) {
      return false;
    }
    ++front;
    --back;
  }
  return true;
}

bool isPangram(string S) {
  size_t location;
  //  if (S.length() < alphabet.length()) {
  if (S.length() < ALPHABET_LENGTH) {
    //not enough letters to be a pangram
    return false;
  }
  //  for(int i = 0; i < alphabet.size(); ++i) {
  //    location = S.find_first_of(alphabet[i]);
  for(char c = 'a'; c <= 'z'; ++c) {
    location = S.find(c);
    if (location == string::npos) {
      return false;
    }
  }
  return true;
}

inline bool isGood(string S) {
  return (isPalindrome(S) && isPangram(S));
}

// find the first string in v begwins with s
// returns the index
int findFirst(vector<string> v, string s) {
  int first = 0;
  int last = v.size();
  int mid = last/2;

  // quick binary search
  while (first < last) {
    mid = ((first + last) / 2) - first;
    if (v[mid] == s) {
      return mid; // exact match must be first
    }
    if (v[mid] < s) {
      first = mid;
    } else {
      last = mid;
    }
  }
  
  return mid;
}

int main() {
  vector<string> wordList;
  vector<string> possibilities;
  string word, s;
  ifstream ins;
  ins.open("WORD.LST2");
  while (ins.good()) {
    getline(ins, word);
    wordList.push_back(word);
  }
  ins.close();
  cerr << wordList.size() << " words being considered" << endl;

  sort(wordList.begin(), wordList.end());
  cerr << "reality check: sorted wordList" << endl;

  vector<string> reversedList;
  for(int i = 0; i < wordList.size(); i++) {
    s = wordList[i];
    reverse(s.begin(), s.end());
    reversedList.push_back(s);
  }

  cerr << "reversed " << reversedList.size() << " words." << endl;
  
  sort(reversedList.begin(), reversedList.end());
  cerr << "sorted the reversed word list" << endl;

  int palindromes = 0;
  int pangrams = 0;
  int append;
  for(int i = 0; i < wordList.size(); i++) {
    if (isPalindrome(wordList[i])) {
      cout << "palindrome: " << wordList[i] << endl;
      ++palindromes;
    } else {
      // append from reverseList the first word which makes a palindrome
      s = wordList[i];
      reverse(s.begin(), s.end());
      for (append = findFirst(reversedList, s.substr(0,2)); append < reversedList.size(); append++) {
	s = wordList[i] + reversedList[append];
	if (isPalindrome(s)) {
	  append = reversedList.size() + 1;
	  cout << "palindrome: " << s << endl;
	}
      }
    }
    if (isPangram(wordList[i])) {
      cout << " pangram: " << wordList[i] << endl;
      ++pangrams;
    }
  }

  cerr << "Palindromes found: " << palindromes << endl;
  cerr << "Pangrams found: " << pangrams << endl;
  return 0;
}
