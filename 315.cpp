#include <iostream>
#include <string>
#include <sstream>
#include <bitset>
#include "primes.h"
using namespace std;

#define uint long long int
#define MIN 10000000
#define MAX 20000000
#define LEDs 7

uint pdr(uint x) {
  uint result = 0;
  while (x > 0) {
    result += x % 10;
    x /= 10;
  }
  return result;
}

string atoi(uint x) {
  ostringstream tmp;
  tmp << x;
  return tmp.str();
}

  /*
     0
    1 2
     3
    4 5
     6

   */
bitset<LEDs> leds[10] = {
  //              6543210
  119, // string("1110111"), // 0
  36,  // string("0010010"), // 1
  93,  // string("1011101"), // 2
  109, // string("1011011"), // 3
  46,  // string("0111010"), // 4
  107, // string("1101011"), // 5
  123, // string("1101111"), // 6
  39,  // string("1110010"), // 7
  127, // string("1111111"), // 8
  111, // string("1111011")  // 9
};

void printDigit(int a) {
  bitset<LEDs> l = leds[a];
  if(l[0]) { cout << "+-+\n"; }
  if(l[1]) { cout << "|."   ; } else { cout << ",,"   ; }
  if(l[2]) { cout << "|\n"  ; } else { cout << ",\n"  ; }
  if(l[3]) { cout << "+-+\n"; }
  if(l[4]) { cout << "|."   ; } else { cout << ",,"   ; }
  if(l[5]) { cout << "|\n"  ; } else { cout << ",\n"  ; }
  if(l[6]) { cout << "+-+\n"; }
}

// simple "start off, turn on x, turn off x, turn on pdr(x), ...
uint sam(int display) {
  if(display == 0) {
    return leds[0].count() * 2;
  }
  uint count = 0;
  int d;
  while (display > 0) {
    d = display % 10;
    display /= 10;
    count += leds[d].count();
  }
  // cout << " ..." << display << "..." << count * 2 << "... ";
  return count * 2;
}

// intelligent "start off, display x, convert to pdr(x), ..., off"
// initial on/off
uint max(int display) {
  return sam(display) / 2;
}

// from x to pdr(x)
// from, to can never be zero (it's not a digital root for any number other than itself
uint max(int from, int to) {
  uint count = 0;
  bitset<LEDs> difference;

  int f, t;
  // "to" will never be longer (or larger) than "from"
  while(to > 0) {
    f = from % 10;
    t = to % 10;
    from /= 10;
    to /= 10;
    
    difference = leds[f] ^ leds[t];
    count += difference.count();
  }

  while(from > 0) {
    f = from % 10;
    from /= 10;
    count += leds[f].count();
  }
  return count;
}

int bigger(int a, int b) {
  return a > b ? a : b;
}

int smaller(int a, int b) {
  return a < b ? a : b;
}

int main() {
  prime p;
  
  int start;
  uint samCount = 0;
  uint maxCount = 0;
  uint lastSam;
  uint lastMax;
  
  int current = start;
  int last = -1;
  for(int start = MIN; start < MAX; ++start) {
    //  for(int start = 1999993; start < 1999994; ++start) {
    if(!p.isPrime(start)) {
      continue;
    }
    cout << "checking " << start << '\n';
    current = start;
    last = -1;
    while(current > 10) {
      samCount += sam(current);
      if(last == -1) {
	maxCount += max(current);
      } else {
	maxCount += max(last, current);
      }
      last = current;
      current = pdr(current);
    }
    lastSam = samCount;
    samCount += sam(current);
    lastMax = maxCount;
    maxCount += max(last, current) + max(current);
    if(lastSam > samCount) {
      cout << "samCount overflowed with current = " << current << endl;
      return 1;
    }
    if(lastMax > maxCount) {
      cout << "maxCount overflowed with current = " << current << endl;
      return 1;
    }
  
    cout << "samCount: " << samCount << '\n' << "maxCount: " << maxCount << '\n';
  }
  
  cout << "Difference: " << samCount - maxCount << endl;

  cout << "with prime p(max):\n" <<
    "real    8m2.570s\n" <<
    "user    7m53.194s\n" <<
    "sys     0m3.303s" << endl;

  cout << "with prime p:";

  return 0;
}
