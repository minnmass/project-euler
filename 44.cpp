#include <iostream>
#include <vector>
#include "pentagonal.h"

using namespace std;


int main() {
  bool done = false;
  pentagonal p;
  unsigned long long int a;
  unsigned long long int b;

  int max = 10000;
  for(int i = 1; i < max; i++) {
    for(int j = i; j < max; j++) {
      a = p.getPentagonal(i);
      b = p.getPentagonal(j);
      if(p.isPentagonal(a + b)) {
	if(p.isPentagonal(b - a)) {
	  cout << "p(" << i << ")=" << a << "; p(" << j << ")=" << b << "; diff=" << b-a << endl;
	  cout << "max p-size:" << p.size() << endl;
	  return 0;
	}
      }
    }
  }

  cout << p.size() << endl;
  return 0;
}
