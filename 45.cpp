#include <iostream>
#include <vector>

using namespace std;

unsigned long long T(unsigned int n) {
  return n*((n+1)/2);
}

unsigned long long P(unsigned int n) {
  return n*(((3*n)-1)/2);
}

unsigned long long H(unsigned int n) {
  return n*((2*n)-1);
}

// with triangle, took .104/0.090 sec

int main() {
  //  vector<unsigned long long> tri;
  vector<unsigned long long> pen;
  vector<unsigned long long> hex;

  //  unsigned int t = 286;
  //  tri.push_back(T(t));
  unsigned int p = 165;
  pen.push_back(P(p));
  unsigned int h = 144;
  hex.push_back(H(h));

  bool done = false;
  while(!done) {
//     while(tri[tri.size() - 1] < hex[hex.size() - 1]) {
//       ++t;
//       tri.push_back(T(t));
//     }
//     if(tri[tri.size() - 1] == hex[hex.size() - 1]) {
      while(pen[pen.size() - 1] < hex[hex.size() - 1]) {
	++p;
	pen.push_back(P(p));
      }
      if(pen[pen.size() - 1] == hex[hex.size() - 1]) {
	done = 1;
	cout << hex[hex.size() - 1] << endl;
	return 0;
      }
      //    }
    ++h;
    hex.push_back(H(h));
  }
  cout << "error" << endl;
  return -1;
}
