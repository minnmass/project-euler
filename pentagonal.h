#ifndef PENTAGONAL_H
#define PENTAGONAL_H

#include <vector>
using namespace std;

class pentagonal {
 private:
  vector<unsigned long long int> v;
  bool add_to_index(unsigned long long int x);
  bool add_to_target(unsigned long long int x);
  unsigned long long int p(unsigned long long int x);
 public:
  pentagonal();
  bool isPentagonal(unsigned long long int x);
  unsigned long long int getPentagonal(unsigned long long int index);
  int size();
};

int pentagonal::size() {
  return v.size();
}

pentagonal::pentagonal() {
  v.push_back(0);
}

unsigned long long int pentagonal::p(unsigned long long int x) {
  unsigned long long int p;
  p = 3 * x;
  p -= 1;
  p *= x;
  p /= 2;
  return p;
}

bool pentagonal::add_to_index(unsigned long long int x) {
  while(x >= v.size()) {
    v.push_back(p(v.size()));
  }
}

bool pentagonal::add_to_target(unsigned long long int x) {
  while (v[v.size() - 1] < x) {
    v.push_back(p(v.size()));
  }
}

unsigned long long int pentagonal::getPentagonal(unsigned long long int x) {
  add_to_index(x);
  return v[x];
}

bool pentagonal::isPentagonal(unsigned long long int x) {
  add_to_target(x);
  int front = 0;
  int back = v.size();
  int mid;
  while (front < back) {
    mid = ((back - front) / 2) + front;
    if(v[mid] == x) {
      return true;
    }
    if(v[mid] < x) {
      front = mid + 1;
    } else {
      back = mid - 1;
    }
  }

  return false;
}

#endif
