#include <iostream>

using namespace std;

#define DAYS 30

// 0 = "on time"
// 1 = "late"
// 2 = "absent"
char day[DAYS];

bool isPrize() {
  int counta = 0;
  bool foundl = false;
  for(int i = 0; i < DAYS; ++i) {
    if(day[i] == 1) {
      if(foundl) {
	return false;
      }
      foundl = true;
    }
  }

  for(int i = 0; i < DAYS - 2; ++i) {
    if((day[i] == 2) && (day[i + 1] == 2) && (day[i + 2] == 2)) {
      return false;
    }
  }

  return true;
}

int main() {
  unsigned long long int prizes = 0;
  unsigned long long int checked = 0;
  for(day[0] = 0; day[0] < 3; ++day[0])
    for(day[1] = 0; day[1] < 3; ++day[1])
      for(day[2] = 0; day[2] < 3; ++day[2])
	for(day[3] = 0; day[3] < 3; ++day[3])
	  for(day[4] = 0; day[4] < 3; ++day[4])
	    for(day[5] = 0; day[5] < 3; ++day[5])
	      for(day[6] = 0; day[6] < 3; ++day[6])
		for(day[7] = 0; day[7] < 3; ++day[7])
		  for(day[8] = 0; day[8] < 3; ++day[8])
		    for(day[9] = 0; day[9] < 3; ++day[9])
		      for(day[10] = 0; day[10] < 3; ++day[10])
			for(day[11] = 0; day[11] < 3; ++day[11])
			  for(day[12] = 0; day[12] < 3; ++day[12])
			    for(day[13] = 0; day[13] < 3; ++day[13])
			      for(day[14] = 0; day[14] < 3; ++day[14])
				for(day[15] = 0; day[15] < 3; ++day[15])
				  for(day[16] = 0; day[16] < 3; ++day[16])
				    for(day[17] = 0; day[17] < 3; ++day[17])
				      for(day[18] = 0; day[18] < 3; ++day[18])
					for(day[19] = 0; day[19] < 3; ++day[19])
					  for(day[20] = 0; day[20] < 3; ++day[20])
					    for(day[21] = 0; day[21] < 3; ++day[21])
					      for(day[22] = 0; day[22] < 3; ++day[22])
						for(day[23] = 0; day[23] < 3; ++day[23])
						  for(day[24] = 0; day[24] < 3; ++day[24])
						    for(day[25] = 0; day[25] < 3; ++day[25])
						      for(day[26] = 0; day[26] < 3; ++day[26])
							for(day[27] = 0; day[27] < 3; ++day[27])
							  for(day[28] = 0; day[28] < 3; ++day[28])
							    for(day[29] = 0; day[29] < 3; ++day[29]) {
							      ++checked;
							      if(isPrize()) {
								++prizes;
							      }
							    }
  
  cout << "Found " << prizes << " prize strings." << endl;
  cout << "Checked " << checked << " of 'em." << endl;
  return 0;
}
