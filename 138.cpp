#include <iostream>
#include <math.h>

using namespace std;

int main() {
  unsigned long long int found = 0;

  unsigned long long int b = 16;
  unsigned long long int l;
  unsigned long long int h;
  unsigned long long int bsquared;
  unsigned long long int sum = 0;
  bool done;

  while(found < 1) {
    l = (b/2) - 1;
    bsquared = ((b/2)*(b/2));
    done = false;
    while (!done) {
      h = sqrt((l*l) - (bsquared));
      if ((h == (l + 1)) || (h == (l - 1))) {
	cout << "found something, l=" << l << ", b=" << b << ", h=" << h << endl;
	found++;
	sum += l;
	done = true;
      } else if (h > l) {
	done = true;
      }
      l++;
    }
    b++;
  }

  cout << found << " triangles were found" << endl;
  cout << "sum of l: " << l << endl;
  return 0;
}
	
