#include <iostream>
#include "primes.h"

using namespace std;

inline unsigned long long int  goldbach(unsigned long long int a, unsigned long long int b) {
  return (a + (2*(b*b)));
}

int main() {
  prime p(100);

  unsigned long long int max = 1000000;
  unsigned long long int a = 0;
  unsigned long long int b = 0;

  bool found;

  for(unsigned long long int i = 3; i < max; i += 2) {
    if(!p.isPrime(i)) {
      found = false;
      // try to find a + b^2 == i; a is prime
      for(int j = 0; (p.at(j) < i) && !found; j++) {
	a = p.at(j);
	b = 1;
	while (goldbach(a, b) < i) {
	  b++;
	}
	if(goldbach(a, b) == i) {
	  found = true;
	}
      }
      if(!found) {
	cout << "no Goldbach found for composite " << i << endl;
	return 0;
      }
    }
  }

  cout << "no non-Goldbach number found; try a higher max than " << max << "?" << endl;
  return 0;
}
