#ifndef HAND_H
#define HAND_H

#include <string>
#include <ostream>

using namespace std;

class hand {
 private:
  string h;
  int val;
  void setValue();
  int getCardValue(int index);
 public:
  hand(string s = "");
  int value();
  int forceValueUpdate();
  string toString() {return h;};
  hand & operator= (const hand input);
  inline int max(int a, int b) {return (a>b)?a:b;};
  bool operator> (const hand other) const;
  bool operator== (const hand other) const;
  bool operator< (const hand other) const;
  friend ostream& operator<< (ostream& os, const hand& hnd);
};

ostream& hand::operator<< (ostream& os, const hand& hnd){
  os << hdn.h;
  return os;
}

bool hand::operator> (const hand other) const {
  return (val > other.val);
}

bool hand::operator== (const hand other) const {
  return (val == other.val);
}

bool hand::operator< (const hand other) const {
  return (val < other.val);
}

int hand::getCardValue(int index) {
  switch(h[index]) {
  case 'T':
    return 10;
  case 'J':
    return 11;
  case 'Q':
    return 12;
  case 'K':
    return 13;
  case 'A':
    return 14;
  }
  return h[index] - '0';
}

void hand::setValue() {
  val = 0;
  for(int i = 0; i < h.size(); i += 3) {
    val = max(val, getCardValue(i));
  }
}

hand::hand(string s) {
  h = s;
  setValue();
}

int hand::value() {
  return val;
}

int hand::forceValueUpdate() {
  setValue();
  return val;
}

hand & hand::operator= (const hand input) {
  h = input.h;
  setVal();
  return this;
}
  
#endif

/*
  poker hands, from best to worst
  
  royal flush
  straight flush
  4 of a kind
  full house
  straight
  3 of a kind
  2 pair
  pair
  high card
*/
