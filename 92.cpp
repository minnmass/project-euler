#include <iostream>
#include <vector>

// #define MAX 10
#define MAX 10000000 
// #define uint unsigned long long int

using namespace std;

struct element{
  bool seen;
  bool finds89;
};

int process(int value) {
  int ret = 0;
  int tmp;
  while(value > 0) {
    tmp = value % 10;
    ret += tmp * tmp;
    value /= 10;
  }
  return ret;
}

int main() {
  // niave?
  int tmp;
  int found = 0;
  for(int i = 1; i < MAX; ++i) {
    tmp = i;
    while(true) {
      if(tmp == 1) {
	break;
      }
      if(tmp == 89) {
	++found;
	break;
      }
      tmp = process(tmp);
    }
  }

  cout << "found " << found << " numbers." << endl;

  return 0;
  // non-niave?
  cout << "starting" << endl;
  uint start = 44;
  int to89 = 0;

  vector<element> values;
  element tmpElement;
  tmpElement.seen = false;
  tmpElement.finds89 = false;
  for(int i = 0; i < MAX; i++) {
    values.push_back(tmpElement);
  }
  values[0].seen = true;
  values[0].finds89 = false;
  values[1].seen = true;
  values[1].finds89 = false;
  values[89].seen = true;
  values[89].finds89 = true;

  cout << "got values" << endl;

  // int tmp;
  for(int i = 1; i < MAX; i++) {
    // tmp = i;
    // while((tmp != 89) && (tmp != 1)) {
    //   cout << tmp << " -> ";
    //   tmp = process(tmp);
    // }
    // cout << tmp << endl;  

    if(values[i].seen) {
      continue;
    }
    tmp = i;
    do {
      tmp = process(tmp);
    } while (!values[tmp].seen);
    if(values[tmp].finds89) {
      values[i].finds89 = true;
      ++to89;
    } else {
      values[i].finds89 = false;
    }
    values[i].seen = true;
  }

  cout << "Found " << to89 << " values that go to 89." << endl;
}
